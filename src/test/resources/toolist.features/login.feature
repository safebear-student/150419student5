Feature: Login
  In order to access the website
  As a user
  I want to know if my login is successful

  Rules:
  - The User must be informed if the login information is incorrect
  - The User must be informed if the login is successful
  - There must be an option to reset the user password

  Glossary:
  - User : Someone who want to create a Tools List using our application
  - Supporters: This is what the customer calls 'Admin' users.

  Questions:
  - Do users get locked out with too many attempts

  @HighRisk
  @HighImpact
  @Smoke
  Scenario Outline: A user logs into the application
    Given I navigate to the login page
    When I enter the login details for a '<userType>'
    Then I can see the following message: '<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |