package com.safebear.auto.bddTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class StepDefs {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    @AfterMethod
    public void tearDown() {
        try {
            Thread.sleep((Integer.parseInt(System.getProperty("sleep", "2000"))));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        //step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedpagetitle(), "We're not on the Login Page or its title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_User(String userType) throws Throwable {
        // Login into the application and check it's as expected
//        switch (userType) {
//            case "validUser":
//                loginPage.enterUsername("tester");
//                loginPage.enterPassword("letmein");
//                loginPage.clickLoginButton();
//                break;
//            case "invalidUser":
//                loginPage.login("hack", "tdegyfipoekgdmddiswgojrkegpfdie-9wrjgrmewpdsjpfjqjef-wj");
//                break;
//            default:
//                Assert.fail("The test data is wrong - the only values that can be accepted are 'validUser' or 'invalidUser' ");
//                break;
        String username;
        String password;
        switch (userType) {
            case "validUser":
                username = "tester";
                password = "letmein";
                break;
            case "invalidUser":
                username = "hack";
                password = "tdegyfipoekgdmddiswgojrkegpfdie-9wrjgrmewpdsjpfjqjef-wj";
                break;
            default:
                username = "";
                password = "";
                break;
        }
        loginPage.login(username, password);

    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_message(String validationMessage) throws Throwable {
//Check the message if login is successful
        switch (validationMessage) {
            case "Username or Password is incorrect":
                Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedpagetitle(), "We're on the wrong page");
                Assert.assertTrue(loginPage.checkvalidationWarningMessage().contains(validationMessage));
                break;
            case "Login Successful":
                Assert.assertEquals(toolsPage.getPageTitle(), toolsPage.getExpectedpagetitle(), "We're on the wrong page");
                Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(validationMessage));
                break;
            default:
                Assert.fail("The test data is wrong");
                break;
        }
    }
}
