package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {
    @Test
    public void testEmployee(){

        //This is where we create an employee objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        //Create a sales employee object
        SalesEmployee victoria = new SalesEmployee();

        /* Print out the status of Hannah and Bob to the screen before employing or firing them */
        System.out.println("Hannah employment state : "+ hannah.employed);
        System.out.println("Bob's Salary  : "+ bob.salary);
        System.out.println("Victoria's salary : " + victoria.getSalary());
        System.out.println("Victoria's Car is : " + victoria.car);
        System.out.println("\n");

        //This is where we employ hannah and fire bob
        hannah.employ();
        bob.givePayRise(1000);
        victoria.setSalary(10000);

        //This is where we employ Victoria and give her a Tesla
        victoria.employ();
        victoria.changeCar("Tesla - Model S");

        //Print out the status of Hannah and Bob to the screen
        System.out.println("Hannah employment state : "+ hannah.employed);
        System.out.println("Bob's Salary : "+ bob.salary);
        System.out.println("Victoria employment state : " + victoria.isEmployed());
        System.out.println("Victoria's salary : " + victoria.getSalary());
        System.out.println("Victoria's car is : " + victoria.car);
        System.out.println("\n");


    }
}
