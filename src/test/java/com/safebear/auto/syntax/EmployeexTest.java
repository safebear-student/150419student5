package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeexTest {
    @Test
    public void testEmployee(){

        //This is where we create an employee objects
        Employeex hannah = new Employeex();
        Employeex bob = new Employeex();
        //Create a sales employee object
        SalesEmployee victoria = new SalesEmployee();

        /* Print out the status of Hannah and Bob to the screen before employing or firing them */
        System.out.println("Hannah employment state : "+ hannah.isEmployed());
        System.out.println("Bob's Salary  : "+ bob.getSalary());
        System.out.println("Victoria's Car is : " + victoria.car);
        System.out.println("\n");

        //This is where we employ hannah and fire bob
        hannah.employ();
        bob.givePayRise(1000);

        //This is where we employ Victoria and give her a Tesla
        victoria.employ();
        victoria.changeCar("Tesla - Model S");

        //Print out the status of Hannah and Bob to the screen
        System.out.println("Hannah employment state : "+ hannah.isEmployed());
        System.out.println("Bob's Salary : "+ bob.getSalary());
        System.out.println("Victoria employment state : " + victoria.isEmployed());
        System.out.println("Victoria's car is : " + victoria.car);
        System.out.println("\n");


    }
}
