package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {
    //Check success message
    private By successfulLoginMessageLocator = By.xpath(".//body/div[@class = 'container']/p/b");
    private By searchFieldLocator = By.id("toolName");
    private By searchButtonLocator = By.className("btn btn-info");

}

