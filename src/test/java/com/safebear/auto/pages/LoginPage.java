package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
@Data
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();
    private String expectedpagetitle = "Login Page";

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void enterUsername(String username) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password) {
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(locators.getLoginButtonLocator()).click();
    }

    public void checkRememberMeStatus(){
        driver.findElement(locators.getRememberMeCheckBoxLocator()).isEnabled();
    }

    public String checkvalidationWarningMessage(){
        return driver.findElement(locators.getValidationWarningMessageLocator()).getText();
    }

    public void login(String username, String password){
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();
    }
}
