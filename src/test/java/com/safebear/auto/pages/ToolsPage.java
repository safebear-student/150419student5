package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
@Data
public class ToolsPage {

    @NonNull WebDriver driver;
    ToolsPageLocators locators = new ToolsPageLocators();
    private String expectedpagetitle = "Tools Page";


    public String getPageTitle() {
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage() {
        return driver.findElement(locators.getSuccessfulLoginMessageLocator()).getText();
    }

    public void enterSearchText(String searchtext) {
        driver.findElement(locators.getSearchFieldLocator()).sendKeys(searchtext);
    }

    public void clickSearchButton() {
        driver.findElement(locators.getSearchButtonLocator()).click();
    }
}

