package com.safebear.auto.autoTests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void loginAsValidUserAndCheckMessageTest() {
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedpagetitle());
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        Assert.assertEquals(toolsPage.getPageTitle(), toolsPage.getExpectedpagetitle());

    }

    @Test
    public void loginAsInvalidUserTest(){
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedpagetitle());
        loginPage.enterUsername("hack");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedpagetitle());

    }

}